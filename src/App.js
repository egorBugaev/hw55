import React, { Component } from 'react';
import './App.css';
import Counter from "./counter/counter";
import Card from "./card/card";

class App extends Component {
    state = {
        cards: [],
        count: 0,
        work: true
    };
    reset = ()=>{
        const count = 0;
        const work = true;
        this.setState({count, work});
        this.addCards();

    };
    addCards = ()=>{
        const cards = [];

            for (let i = 0; i < 36; i++) {
                let card_x = {};
                card_x.id = i;
                card_x.showCard = true;
                card_x.hasGift = false;
                card_x.presed = false;
                cards.push(card_x);
            }

            cards[Math.floor(Math.random() * 36)].hasGift = true;
            this.setState({cards});


    };

    componentDidMount(){this.addCards()}

    removeCard= (id) => {
        if(this.state.work) {
            const index = this.state.cards.findIndex(p => p.id === id);
            let count = this.state.count;
            let cards = [...this.state.cards];
            let work =this.state.work;
            cards[index].showCard = false;
            if(!this.state.cards[index].presed){
                cards[index].presed = true;
                count++;
            }
            this.setState({cards, count});
            if (cards[index].hasGift) {
                alert('You win');
                work = false;
                this.setState({work});
            }
        }
    };

    render() {
        console.log(this.state);
        let card = this.state.cards.map(card => {return <Card
                card={card}
                id={card.id}
                key={card.id}
                remove={() => this.removeCard(card.id)}
                hasGift ={card.hasGift}
                show={card.showCard}
            />});

        return (
            <div className="App">
                {card}
                <Counter count ={this.state.count}/>
                <button onClick={this.reset}>reset</button>
            </div>
        );
    }
}

export default App;
