import React from 'react'

const Counter = props=> {

    return (
        <p>
            Total tries: {props.count}
        </p>
    )
};

export default Counter