import React from 'react';

const Card = props => {
    let className = 'greyBlock';
    let gift = null;
    if(!props.show){
        className = 'transparentBlock'
        if(props.hasGift){
            gift = 'o';
        }
    }

    return (
        <div className={className} onClick={props.remove}>{gift}</div>
    )
};

export default Card;

